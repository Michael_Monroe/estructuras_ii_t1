##					TAREA 1: Predictores de saltos condicionales**

En esta tarea se implementan cuatro métodos para la predicción de branches:
*  Predictor Bimodal: Utiliza una tabla llamada Branch History Table (BHT), que funciona como un contador de dos bits.
*  Predictor GShare: Utiliza la historia global de las instrucciones de saltos para seleccionar un contador de dos bits en una BHT.
*  Predictor PShare: Utiliza la historia privada de las instrucciones de salto. También utiliza una tabla llamada Pattern History Table (PHT).
*  Predictor por Torneo: Utiliza un metapredictor para predecir cual predictor servirá mejor (PShare o GShare).
  
--------------------------------------------
--------------------------------------------

**Dependencias**

*  En el directorio `/B54557_T1/include` encontrará los headers del código fuente.
*  En el directorio `/B54557_T1/scr` encontrará los archivos del código fuente.
*  Para compilar el archivo necesitará gcc.


--------------------------------------------
--------------------------------------------

**Compilación** 

En la terminal de comandos en el directorio `/B54557_T1`:

1. Ejecutar la instrucción `make`
2. Encontrará el ejecutable creado en el directorio `/B54557_T1/simulation` con el nombre `branch`. 

--------------------------------------------
--------------------------------------------

**Ejecución**

En la terminal de comandos en el directorio `/B54557_T1/simulation` escribir:

`gunzip -c branch-trace-gcc.trace.gz | ./branch -bp <#> -s <#> -gh <#> -ph <#> -o <#>`


## Flags
---

* -s  : Tamaño de la tabla BTH (size = 2^s).
* -bp : Tipo de predicción (Bimodal = 0, PShare = 1, GShare = 2, Tournament = 3).
* -gh : Tamaño de registro de historia global.
* -ph : Tamaño de registrso de historia privada.
* -o  : Salida de la simulación (1 para generar documento de salida o 0 para no generarlo ).


## Outs
---
* En el directorio `/B54557_T1/simulation/output` encontrará los archivos de salida, si estos fueron generados y con el nombre del respectivo tipo de simulador usado. 



## Inputs
---
* En el directorio `/B54557_T1/simulation` encontrará Trace comprimido, si desea usar un Trace diferente sustituya el archivo.


