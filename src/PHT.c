#include "PHT.h"

PHT* createPHT(unsigned int State0, unsigned int size, unsigned int bH){
  PHT* pointP = malloc(sizeof(PHT));
  if (pointP != NULL) { //Memory allocated for the PHT struct
    pointP->size = 1<<size; //Entries of the PHT.
    pointP->array = malloc(pointP->size * sizeof(HPred*));
    if (pointP->array != NULL) { //Memory allocated for PHT->array
      for (unsigned int i = 0; i < pointP->size; i++) {
        pointP->array[i] = createHPred(State0, bH); //Create a history register for each PHT entry.
        if (pointP->array[i] == NULL) {
          return 0; //Error
        }
      }
      return pointP; //Return the pointer of the PHT struct
    }
    else {
      return 0; //Error
    }
  }
  else {
    return 0; //Error
  }
}

unsigned int getPHT(PHT* pointP, unsigned int i){
  return pointP->array[i]->Hist;
}

void setPHT(PHT* pointP, unsigned int i, short actualB){
  setHist(pointP->array[i], actualB);
}

void freePHT(PHT* pointP){
  for (unsigned int i = 0; i < pointP->size; i++) {
    freeHPred(pointP->array[i]);
  }
  free(pointP->array);
  free(pointP);
}

