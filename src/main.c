#include "simulator.h"

int main(int argc, char const *argv[]) {

  //Initialize the simulation parameters and statistics.
  params P0 = getParams(argc, argv);
  stats S0 = createStats();
  traceLine TL0;
  PredOut O0;

char* fileName;

if(P0.o){

  switch (P0.bp) {
    case 0: fileName = "output/Bimodal.txt"; break;
    case 1: fileName = "output/PShare.txt"; break;
    case 2: fileName = "output/GShare.txt"; break;
    case 3: fileName = "output/Tournament.txt"; break;
    default: fileName = "output/outfile.txt";
  }
}


FILE* file_ptr;
  if(P0.o){
    file_ptr = fopen(fileName, "w");
    if (file_ptr == NULL) {
        printf("Error output file not saved.\n");
        P0.o = 0;
        }
    else {
        fprintf(file_ptr, "\tPC\t|    Outcome\t|   Prediction\t|  Correct/Incorrect\n");
        }
      }


  // Bimodal Predictor:

  if (P0.bp == 0) {
    BHT* pointT = createBHT(SN, P0.s);
    if (pointT != NULL) {
      int i = 0;

      while (getTraceLine(&TL0)) {
        simulateBimodal(pointT, TL0.pc, TL0.actualB, &O0);
        setStats(&S0, &O0);

        if(P0.o && i < 5000){
          char predChar = (O0.prediction) ? 'T' : 'N';
          char* outStr = (O0.result) ? "Correct" : "Incorrect";
          fprintf(file_ptr, "%u\t|\t%c\t|\t%c\t|\t%s\n", TL0.pc, TL0.actualBChar, predChar, outStr);
          i++;
        }
      }

      freeBHT(pointT);
    } else {
      printf("Simulation failed.\n");
    }
  }


  // PShare Predictor:

  else if (P0.bp == 1) {
    BHT* pointT = createBHT(SN, P0.s);
    PHT* pointP = createPHT(0, P0.s, P0.ph);

    if (pointT != 0 && pointP != 0) {
      int i = 0;

      while (getTraceLine(&TL0)) {
        simulatePShare(pointT, pointP, TL0.pc, TL0.actualB, &O0);
        setStats(&S0, &O0);

        if(P0.o && i < 5000){
          char predChar = (O0.prediction) ? 'T' : 'N';
          char* outStr = (O0.result) ? "Correct" : "Incorrect";
          fprintf(file_ptr, "%u\t|\t%c\t|\t%c\t|\t%s\n", TL0.pc, TL0.actualBChar, predChar, outStr);
          i++;
        }
      }

      freeBHT(pointT);
      freePHT(pointP);
    } else {
      printf("Simulation failed.\n");
    }
  }


  // GShare Predictor:

  else if (P0.bp == 2) {
    BHT* pointT = createBHT(SN, P0.s);
    HPred* pointH = createHPred(0, P0.gh);

    if (pointT != 0 && pointH != 0) {
      int i = 0;

      while (getTraceLine(&TL0)) {
        simulateGShare(pointT, pointH, TL0.pc, TL0.actualB, &O0);
        setStats(&S0, &O0);

        if(P0.o && i < 5000){
          char predChar = (O0.prediction) ? 'T' : 'N';
          char* outStr = (O0.result) ? "Correct" : "Incorrect";
          fprintf(file_ptr, "%u\t|\t%c\t|\t%c\t|\t%s\n", TL0.pc, TL0.actualBChar, predChar, outStr);
          i++;
        }
      }

      freeBHT(pointT);
      freeHPred(pointH);
    } else {
      printf("Simulation failed.\n");
    }
  }

  // Tournament Predictor:

  else if (P0.bp == 3) {

    BHT* pointT_G = createBHT(SN, P0.s);
    HPred* pointH_G = createHPred(0, P0.gh);

    BHT* pointT_P = createBHT(SN, P0.s);
    PHT* pointP_P = createPHT(0, P0.s, P0.ph);

    BHT* pointT_M = createBHT(SP, P0.s);

    if (pointT_G != 0 && pointH_G != 0 && pointT_P != 0 && pointP_P != 0 && pointT_M != 0) {
      int i = 0;

      while (getTraceLine(&TL0)) {
        simulateTournament(pointT_G, pointH_G, pointT_P, pointP_P, pointT_M, TL0.pc, TL0.actualB, &O0);
        setStats(&S0, &O0);

        if(P0.o && i < 5000){
          char predChar = (O0.prediction) ? 'T' : 'N';
          char* outStr = (O0.result) ? "Correct" : "Incorrect";
          fprintf(file_ptr, "%u\t|\t%c\t|\t%c\t|\t%s\n", TL0.pc, TL0.actualBChar, predChar, outStr);
          i++;
        }
      }

      freeBHT(pointT_G);
      freeHPred(pointH_G);
      freeBHT(pointT_P);
      freePHT(pointP_P);
      freeBHT(pointT_M);
    } else {
      printf("Simulation failed.\n");
    }
  }


if (P0.o) {
      fclose(file_ptr);
    }

  printParams(P0);
  printResults(&S0);
  return 0;
}
