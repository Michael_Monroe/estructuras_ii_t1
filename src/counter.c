#include "counter.h"

counter* createCounter(short State0){
  counter* pointC = malloc(sizeof(counter));
  if (pointC != NULL) { //memory was assigned
    if (State0 == ST || State0 == WT || State0 == WN || State0 == SN ) {
      pointC->state = State0;
      pointC->prediction = makePrediction(pointC);
      return pointC;
    } else {
      return 0; //Error.
    }
  } else { //Error.
    return 0;
  }
}

void freeCounter(counter* pointC){
  free(pointC);
}

short makePrediction(counter* pointC){
  return (pointC->state == ST || pointC->state == WT); //Return 1 if(state == ST or WT) else 0
}

bool evalCounter(counter* pointC, short actualB){
  return (pointC->prediction == actualB); //Return true if prediction match with actualB
}

void changeStateC(counter* pointC, short actualB){
  if (actualB == T && pointC->state < ST) { //State increase if actualB == T and state != ST.
    pointC->state += 1;
  }
  if (actualB == N && pointC->state > SN){ //State decrease if actualB == N and state != SN
    pointC->state -= 1;
  }

  // Make the new prediction
  pointC->prediction = makePrediction(pointC);
}

char* State2str(counter* pointC){

  switch (pointC->state) {
    case SN: return "SN";
    case WN: return "WN";
    case WT: return "WT";
    case ST: return "ST";
    default: return "E!";
  }
}

char branch2char(short actualB){
  return (actualB == T) ? 'T' : 'N';
}

short char2branch(char actualB){
  return (actualB == 'T') ? T : N;
}
