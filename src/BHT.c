#include "BHT.h"

BHT* createBHT(short State0, unsigned int size){
  BHT* pointT = malloc(sizeof(BHT));
  if (pointT != NULL) { //memory allocated
    pointT->size = 1<<size; //BHT entries 2^s.
    pointT->array = malloc(pointT->size * sizeof(counter*));
    if (pointT->array != NULL) { //memory allocated for BHT->array
      for (unsigned int i = 0; i < pointT->size; i++) {
        pointT->array[i] = createCounter(State0); //Create a counter for each entry.
        if (pointT->array[i] == NULL) {
          return 0; //Error
        }
      }
      return pointT; //Return the pointer of the BHT created
    }
     else {
      return 0; //Error
    }
  }
   else {
    return 0; //Error
  }
}

short getPrediction(BHT* pointT, unsigned int i){
  return pointT->array[i]->prediction;
}

bool evalPrediction(BHT* pointT, unsigned int i, short actualB){
  counter* prediction = pointT->array[i];
  return evalCounter(prediction, actualB);
}

void changeState(BHT* pointT, unsigned int i, short actualB){
  counter* pointC = pointT->array[i];
  changeStateC(pointC, actualB);
}

void freeBHT(BHT* pointT){
  for (unsigned int i = 0; i < pointT->size; i++) {
    freeCounter(pointT->array[i]);
  }
  free(pointT->array);
  free(pointT);
}
