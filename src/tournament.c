#include "tournament.h"

short pickTPred(BHT* pointM, unsigned int i, short PPred, short GPred){
  counter* MetaPick = pointM->array[i];
  switch (makePrediction(MetaPick)) {
    case 0: return GPred;
    case 1: return PPred;
    default: return -1;
  }
}

void changeMeta(BHT* pointM, unsigned int i, bool Pout, bool Gout){
  if (Gout != Pout) {
    counter* MetaPick = pointM->array[i];
    if (Gout && MetaPick->state > SG) {
      MetaPick->state -= 1;
    }
    if (Pout && MetaPick->state < SP) {
      MetaPick->state += 1;
    }
  }
}
