#include "simulator.h"

params getParams(int argc, char const* argv[]){
  params P0;
  //Default values:
  P0.s = 0;
  P0.bp = 4;
  P0.gh = 0;
  P0.ph = 0;
  P0.o = 0;

  for (int i = 1; i < argc; i = i + 2) {
    if (strcmp("-s", argv[i]) == 0) {
      P0.s = atoi(argv[i+1]);
    }
    else if (strcmp("-bp", argv[i]) == 0) {
      P0.bp = atoi(argv[i+1]);
    }
    else if (strcmp("-gh", argv[i]) == 0) {
      P0.gh = atoi(argv[i+1]);
    }
    else if (strcmp("-ph", argv[i]) == 0) {
      P0.ph = atoi(argv[i+1]);
    }
    else if (strcmp("-o", argv[i]) == 0) {
      P0.o = atoi(argv[i+1]);
    }
  }
  return P0;
}

void printParams(params P0){
  char* line = "---------------------------------------------------------------------------";
  printf("%s\n%s\n%s\n",line, "Prediction parameters:", line);

  char* TPred;
  switch (P0.bp) {
    case 0: TPred = "Bimodal"; break;
    case 1: TPred = "PShare"; break;
    case 2: TPred = "GShare"; break;
    case 3: TPred = "Tournament"; break;
    default: TPred = "Invalid type of predictor";
  }

  printf("Branch prediction type: \t\t\t\t %s\n", TPred);
  printf("BHT size (entries): \t\t\t\t\t %d\n", 1 << P0.s);
  printf("Global history register size: \t\t\t\t %d\n", P0.gh);
  printf("Private history register size: \t\t\t\t %d\n", P0.ph);
  printf("%s\n", line);
}

stats createStats(){
  stats S0;
  S0.Nbranches = 0;
  S0.TC = 0;
  S0.TI = 0;
  S0.NTC = 0;
  S0.NTI = 0;
  return S0;
}

void setStats(stats* S0, PredOut* O0){
  S0->Nbranches += 1;
  if (O0->prediction == T) { //Take predited
    if (O0->result) {  //Correct predictions
      S0->TC += 1;
    } else { //Incorrect predictions
      S0->NTI += 1;
    }
  } else { //Not Take predicted
    if (O0->result) { //Correct predictions
      S0->NTC += 1;
    } else { //Incorrect predictions
      S0->TI += 1;
    }
  }
}

void printResults(stats* S0){
  float C = (float) (S0->TC + S0->NTC);
  float CP = C/S0->Nbranches * 100; //Percentage
  char* line = "---------------------------------------------------------------------------";
  printf("%s\n%s\n%s\n", line, "Simulation results:", line);
  printf("Number of branch: \t\t\t\t\t %d\n", S0->Nbranches);
  printf("Number of correct prediction of taken branches: \t %d\n", S0->TC);
  printf("Number of incorrect prediction of taken branches: \t %d\n", S0->TI);
  printf("Correct prediction of not taken branches: \t\t %d\n", S0->NTC);
  printf("Incorrect prediction of not taken branches: \t\t %d\n", S0->NTI);
  printf("Percentage of correct predictions: \t\t\t %f\n", CP);
  printf("%s\n", line);
}

bool getTraceLine(traceLine* TL0){
  int bufsize = 32;
  char buffer[bufsize];
  if (fgets(buffer, bufsize, stdin)) {
    TL0->pc = atoi(strtok(buffer, " "));
    TL0->actualBChar = strtok(NULL, " ")[0];
    TL0->actualB = (TL0->actualBChar == 'T') ? T : N;
    return true;
  } else {
    return false;
  }
}



void simulateBimodal(BHT* pointT, unsigned int PC, short actualB, PredOut* O0){

  unsigned int M = pointT->size - 1;
  unsigned int i = PC & M;

  O0->prediction = getPrediction(pointT, i);
  O0->result = evalPrediction(pointT, i, actualB);
  changeState(pointT, i, actualB);
}


void simulateGShare(BHT* pointT, HPred* pointH, unsigned int PC, short actualB, PredOut* O0){

  unsigned int i0 = PC ^ pointH->Hist;
  unsigned int M = pointT->size - 1;
  unsigned int i1 = i0 & M;

  O0->prediction = getPrediction(pointT, i1);
  O0->result = evalPrediction(pointT, i1, actualB);
  setHist(pointH, actualB);
  changeState(pointT, i1, actualB);
}


void simulatePShare(BHT* pointT, PHT* pointP, unsigned int PC, short actualB, PredOut* O0){

  unsigned int M = pointT->size - 1;
  unsigned int ip = PC & M;
  unsigned int H = getPHT(pointP, ip);
  unsigned int i0 = PC ^ H;
  unsigned int i1 = i0 & M;

  O0->prediction = getPrediction(pointT, i1);
  O0->result = evalPrediction(pointT, i1, actualB);
  setPHT(pointP, ip, actualB);
  changeState(pointT, i1, actualB);
}


void simulateTournament(BHT* pointT_G, HPred* pointH_G, BHT* pointT_P, PHT* pointP_P, BHT* pointT_M, unsigned int PC, short actualB, PredOut* O0){
  PredOut Out_G;
  simulateGShare(pointT_G, pointH_G, PC, actualB, &Out_G);
  PredOut Out_P;
  simulatePShare(pointT_P, pointP_P, PC, actualB, &Out_P);

  unsigned int M = pointT_M->size - 1;
  unsigned int i = PC & M;

  O0->prediction = pickTPred(pointT_M, i, Out_P.prediction, Out_G.prediction);
  O0->result = (actualB == O0->prediction);
  changeMeta(pointT_M, i, Out_P.result, Out_G.result);
}
