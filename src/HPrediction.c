#include "HPrediction.h"

HPred* createHPred(unsigned int H0, unsigned int bM){
  HPred* pointH = malloc(sizeof(HPred));
  if (pointH != NULL) {
    pointH->M = (1<<bM) - 1;
    pointH->Hist = H0 & pointH->M;
    return pointH;
  } else {
    return 0;
  }
}

void freeHPred(HPred* pointH){
  free(pointH);
}

void setHist(HPred* pointH, short actualB){
  pointH->Hist = pointH->M & ((pointH->Hist << 1) + actualB);
}

