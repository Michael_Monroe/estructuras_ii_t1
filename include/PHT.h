/**
 * @brief Use of PHT (Pattern History Table).
 * @file PHT.h
 * @author Michael Monroe Mata
 * @date 2020-04-29
 */

 #ifndef PHT_H
 #define PHT_H

#include "HPrediction.h"

 typedef struct PHT{
   HPred** array;
   unsigned int size;
 } PHT;

 PHT* createPHT(unsigned int State0, unsigned int size, unsigned int bH);
 /**
 @brief Allocate memory for PHT struct.
 @param State0 is the initial state.
 @param size is the number of history registers.
 @param bH is the number of bits for history registers of PHT.
 @return Pointer to allocated memory for PHT.
 */

 unsigned int getPHT(PHT* pointP, unsigned int i);
 /**
 @brief Get the history of the HPred in each PHT entry.
 @param pointP is a pointer to the PHT struct.
 @param i is the index to the entry of PHT.
 @return a number representing the history.
 */

 void setPHT(PHT* pointP, unsigned int i, short actualB);
 /**
 @brief Set the history in an entry of PHT.
 @param pointP is a pointer to the PHT struct.
 @param i is the index to the entry of PHT.
 @param actualB is the actual branch outcome of the program.
 */

 void freePHT(PHT* pointP);
 /**
 @brief Free the allocated memory for PHT struct.
 @param pointP is a pointer to the PHT struct to free.
 */

#endif //end of PHT_T
