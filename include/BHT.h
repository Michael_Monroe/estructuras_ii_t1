/**
 * @brief Use of BHT (Branch History Table).
 * @file BHT.h
 * @author Michael Monroe Mata
 * @date 2020-04-29
 */

 #ifndef BHT_H
 #define BHT_H

#include "counter.h"

 typedef struct BHT{
   counter** array;
   unsigned int size;
 } BHT;

 BHT* createBHT(short State0, unsigned int size);


 /**
 @brief Allocate memory for BHT struct.
 @param State0 is the intial state of the counters.
 @param size is the total entries of the array.
 @return Pointer to the allocated memory for BHT.
 */


 short getPrediction(BHT* pointT, unsigned int i);
 /**
 @brief Get the prediction from a BHT.
 @param pointT is a pointer to the BHT struct.
 @param i is the index of the entry to get.
 @return 1 to Take the branch, 0 to Not Take the branch.
 */

 bool evalPrediction(BHT* pointT, unsigned int i, short actualB);
 /**
 @brief Evaluate if the prediction was correct or incorrect.
 @param pointT is a pointer to the BHT struct.
 @param i is the index of the entry to compare.
 @param actualB is the actual branch outcome of the program.
 @return true if the actualB and Prediction match, false if don't match.
 */

 void changeState(BHT* pointT, unsigned int i, short actualB);
 /**
 @brief Change the counter state.
 @param pointT is a pointer to the BHT struct.
 @param i is the index of the entry of the BHT.
 @param actualB is the actual branch outcome of the program.
 */

 void freeBHT(BHT* pointT);
 /**
 @brief Free the allocated memory for BHT struct.
 @param pointT is a pointer to the BHT struct to free.
 */

#endif //end of BHT_T
