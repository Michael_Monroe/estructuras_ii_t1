/**
 * @brief History of predictions made.
 * @file HPrediction.h
 * @author Michael Monroe Mata
 * @date 2020-04-29
 */

#ifndef HPRED_H
#define HPRED_H

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct HPred {
  unsigned int Hist; //History of branchs
  unsigned int M; //Mask for get the index for each PC
} HPred;

HPred* createHPred(unsigned int H0, unsigned int bM);
/**
@brief Create a history of predicttions struct.
@param bM is are the bits for mask.
@param H0 is the initial history.
@return a pointer to the creater HREG structure.
*/

void freeHPred(HPred* pointH);
/**
@brief Free the allocated memory for HPred structure.
@param pointH is a pointer to the HPred to free.
*/

void setHist(HPred* pointH, short actualB);
/**
@brief Set the history of a HPred struct.
@param pointH is a pointer to the HPred to set.
@param actualB is the actual branch outcome of the program..
*/

#endif

