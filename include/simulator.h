/**
 * @brief Use of tables for simulate the predictors.
 * @file simulator.h
 * @author Michael Monroe Mata
 * @date 2020-04-29
 */

#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "BHT.h"
#include "HPrediction.h"
#include "PHT.h"
#include "tournament.h"

typedef struct params{
  unsigned int s; //size of the BHT (entrys = 2^s).
  unsigned int bp; //Prediction type (Bimodal = 0, PShare = 1, Gshare = 2, Tournament = 3).
  unsigned int gh; //size of register for GShare.
  unsigned int ph; //size of register for PShare.
  bool o; //output of the simulation.
} params;

params getParams(int argc, char const* argv[]);
/**
@brief Get the parameters of the exetution instrucction.
@param argc is the number of parameters recieved.
@param argv is a string array with the parameters.
@return A params struct with the parameters of the simulation.
*/

void printParams(params P0);
/**
@brief Print the parameters in the terminal.
@param P0 is a params struct with the parameters of the simulation.
*/

typedef struct stats{
  unsigned int Nbranches; //Number of branches.
  unsigned int TC; //Number of correct prediction of taken branches.
  unsigned int TI; //Number of incorrect prediction of taken branches.
  unsigned int NTC; //Number of correct prediction of not taken branches.
  unsigned int NTI; //Number of incorrect prediction of not taken branches.
} stats;

stats createStats(void);
/**
@brief Create a stats struct with all in 0.
@return A stats struct.
*/

typedef struct PredOut{
  short prediction;
  bool result;
} PredOut;

void setStats(stats* S0, PredOut* O0);
/**
@brief This function updates the statistics according the the current output.
@param S0 is a pointer to the statistics structure that needs to be updated.
@param  is a pointer to the outputData structures that contains the data required to update the statistics.
*/

void printResults(stats* S0);
/**
@brief Prints the stats in terminal.
@param S0 is a pointer to the statis struct.
*/

typedef struct traceLine{
  unsigned int pc;
  short actualB;
  char actualBChar;
} traceLine;

bool getTraceLine(traceLine* TL0);
/**
@brief Get the Trace Line for the simulation.
@param TL0 is a pointer to the traceLine struct.
@return true if a TL0  false if there are no more lines to be read.
*/

void simulateBimodal(BHT* pointT, unsigned int PC, short actualB, PredOut* O0);
/**
@brief Simulate bimodal predictor by an entry of the BHT.
@param pointT is a pointer to BHT struct.
@param PC is the Program Counter.
@param actualB is the actual branch outcome of the program.
@param O0 is a pointer to the PredOut struct.
*/

void simulateGShare(BHT* pointT, HPred* pointH, unsigned int PC, short actualB, PredOut* O0);
/**
@brief Simulate GShare predictor by an entry of the BHT.
@param pointT is a pointer to BHT struct.
@param pointH is a pointer to the histroy register structure that holds the global history.
@param PC is the Program Counter.
@param actualB is the actual branch outcome of the program.
@param O0 is a pointer to the PredOut struct.
*/

void simulatePShare(BHT* pointT, PHT* pointP, unsigned int PC, short actualB, PredOut* O0);
/**
@brief Simulate PShare predictor by an entry of the BHT.
@param pointT is a pointer to BHT struct.
@param pointP is a pointer to the PHT struct.
@param PC is the Program Counter.
@param actualB is the actual branch outcome of the program.
@param O0 is a pointer to the PredOut struct.
*/

void simulateTournament(BHT* pointT_G, HPred* pointH_G, BHT* pointT_P, PHT* pointP_P, BHT* pointT_M, unsigned int PC, short actualB, PredOut* O0);
/**
@brief Simulate Tournament predictor.
@param pointT_G is a pointer to BHT struct for GShare.
@param pointH_G is a pointer to HPred struct for GShare.
@param pointT_P is a pointer to BHT struct for PShare.
@param pointP_P is a pointer to PHT struct for PShare.
@param pointT_M is a pointer to BHT struct for metapredictor.
@param PC is the Program Counter.
@param actualB is the actual branch outcome of the program.
@param O0 is a pointer to the PredOut struct.
*/

#endif
