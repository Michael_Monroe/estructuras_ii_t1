/**
 * @brief  Use BHT to make a metapredictor.
 * @file tournament.h
 * @author Michael Monroe Mata
 * @date 2020-04-29
 */

#ifndef TOURNAMENT_H
#define  TOURNAMENT_H

#ifndef SG //Strong prefer GShared
#define SG 0b00
#endif

#ifndef WG //Weak prefer GShared
#define WG 0b01
#endif

#ifndef WP //Weak prefer PShared
#define WP 0b10
#endif

#ifndef SP //Strong prefer PShared
#define SP 0b11
#endif

#include "BHT.h"

short pickTPred(BHT* pointM, unsigned int i, short PPred, short GPred);
/**
@brief Pick between GShare and PShare predictions by tournament.
@param pointM is a pointer to the BHT struct to the metapredictor.
@param i is the index for get the counter of an entry in BHT.
@param PPred is the prediction from the PShare.
@param GPred is the prediction from the GShare.
@return Return 1 to Take, or 0 to No - Take.
*/

void changeMeta(BHT* pointM, unsigned int i, bool Pout, bool Gout);
/**
@brief Change the state of the status for BHT of PShare  and GShare.
@param i is the index of the entry of the  meta BHT.
@param Pout true if PSHare was correct, else False.
@param Gout true if GSHare was correct, else False.
*/


#endif
