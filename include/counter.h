/**
 * @brief Counters for the predictions.
 * @file counter.h
 * @author Michael Monroe Mata
 * @date 2020-04-29
 */

#ifndef COUNTER_H
#define COUNTER_H

#include <stdlib.h>
#include <stdbool.h>

//Macros for the Status of the counter

#ifndef N //Not Take
#define N 0b0
#endif

#ifndef T //Take
#define T 0b1
#endif

#ifndef SN //Strong Not Take
#define SN 0b00
#endif

#ifndef WN //Weak Not Take
#define WN 0b01
#endif

#ifndef WT //Weak Take
#define WT 0b10
#endif

#ifndef ST //Strong Take
#define ST 0b11
#endif


typedef struct counter{ //Counter struct for the State and Prediction.
  short state;
  short prediction;
} counter;

counter* createCounter(short State0);
/**
@brief Allocate memory for a counter struct.
@param State0 is the initial state of the counter.
@return A pointer to allocated memory for counter.
*/

void freeCounter(counter* pointC);
/**
@brief Free the allocated memory for counter struct.
@param pointC is a pointer to the counter to free.
*/

short makePrediction(counter* pointC);
/**
@brief Make a prediction based on status.
@param pointC is a pointer to the counter struct.
@return 1 if the prediction is Take the branch, 0 for Not - Take.
*/

bool evalCounter(counter* pointC, short actualB);
/**
@brief Evaluate if prediction match with actualB.
@param pointC is a pointer to the counter struct.
@param actualB is actual branch outcome of the program.
@return true if the actualB and Prediction match, false if don't match.
*/

void changeStateC(counter* pointC, short actualB);
/**
@brief Change the state of the counter in pointC.
@param pointC is a pointer to the counter struct.
@param actualB is actual branch outcome of the program.
*/

char* State2str(counter* pointC);
/**
@brief Traslate status to string type.
@param pointC is a pointer to the counter struct.
@return String with state of pointC*.
*/

char branch2char(short actualB);
/**
@brief Traslate branch to char type.
@param actualB is actual branch outcome of the program.
@return char 'T' if actualB is Take or 'N' if Not - Take.
*/

short char2branch(char outcome);
/**
@brief Traslate char type to branch.
@param actualB is actual branch outcome of the program.
@return String with state of pointC*.
*/

#endif
