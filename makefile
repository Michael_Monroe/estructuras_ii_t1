#-----> Paths 
SRCDIR := ./src
INCDIR := ./include
OBJDIR := ./simulation
TESTDIR := ./tests
TARGET := branch

#-----> Files 
SRCFILES 		:= $(wildcard $(SRCDIR)/*.c)


#-----> Flags
CC = gcc
CFLAGS = -Wall -std=c99 -I $(INCDIR)


#-----> Rules
all: $(OBJDIR)/$(TARGET)

$(OBJDIR)/$(TARGET):
	$(CC) $(CFLAGS) -o $(OBJDIR)/$(TARGET) $(SRCFILES)


clean:
	rm -f $(OBJDIR)/$(TARGET)
	rm -f $(OBJDIR)/*.o
	rm -f $(OBJDIR)/outputFile.txt
	rm -rf $(OBJDIR)/branch.dSYM

